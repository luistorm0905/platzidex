package com.platzi.platzidex.utiltest

import com.platzi.platzidex.util.moveAUnit
import org.junit.Test
import kotlin.test.assertEquals

class DoubleUtilsTest {
    @Test
    fun `move a unit`() {
        val testDouble: Double = 100.0
        assertEquals(testDouble.moveAUnit(), 10.0)
    }
}