package com.platzi.platzidex

import android.app.Application
import com.platzi.platzidex.di.DataBaseModule
import com.platzi.platzidex.di.PokeDetailModule
import com.platzi.platzidex.di.PokeListModule
import com.platzi.platzidex.di.RetrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin

class PlatziDexApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@PlatziDexApplication)
            modules(listOf(RetrofitModule, PokeListModule, PokeDetailModule, DataBaseModule))
        }
    }
}