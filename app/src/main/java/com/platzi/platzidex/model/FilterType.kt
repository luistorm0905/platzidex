package com.platzi.platzidex.model

data class FilterType(
    val name: String = "",
    var isSelected: Boolean = false
)
