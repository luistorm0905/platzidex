package com.platzi.platzidex.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.platzi.domain.repository.PokeApiRepository
import com.platzi.domain.utils.Resource
import kotlinx.coroutines.Dispatchers

class PokemonListViewModel(private val pokeApiRepository: PokeApiRepository) : ViewModel() {

    fun getPokemonList() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = pokeApiRepository.getPokemonList()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: ""))
        }
    }

    fun getFilteredList(type: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = pokeApiRepository.getPokemonByType(type)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: ""))
        }
    }

    fun getSpecificPokemonInfo(url: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = pokeApiRepository.getSpecificPokemonInfo(url)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: ""))
        }
    }

}