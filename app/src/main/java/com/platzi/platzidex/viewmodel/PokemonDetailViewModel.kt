package com.platzi.platzidex.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.platzi.domain.repository.PokeApiRepository
import com.platzi.domain.utils.Resource
import kotlinx.coroutines.Dispatchers

class PokemonDetailViewModel(private val pokeApiRepository: PokeApiRepository) : ViewModel() {

    fun getSpeciesInfo(url: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = pokeApiRepository.getSpeciesInfo(url)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: ""))
        }
    }

    fun getAbilityInfo(url: String, index: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = pokeApiRepository.getAbilitiesInfo(url).also {
                it.index = index
            }))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: ""))
        }
    }

    fun getEvolutionChain(url: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = pokeApiRepository.getEvolutionChain(url)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: ""))
        }
    }

}