package com.platzi.platzidex.util

enum class PokeApiLanguages(val value: String) {
    SPANISH("es"),
    ENGLISH("en")
}