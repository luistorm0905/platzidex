package com.platzi.platzidex.util

import com.platzi.platzidex.R

class TypeBackground {
    companion object {
        fun getTypeBackground(type: String) : Int {
            return when (type.lowercase()) {
                "water" -> R.color.water
                "fire" -> R.color.fire
                "grass" -> R.color.grass
                "electric" -> R.color.electric
                "ice" -> R.color.ice
                "fighting" -> R.color.fighting
                "poison" -> R.color.poison
                "ground" -> R.color.ground
                "flying" -> R.color.flying
                "psychic" -> R.color.psychic
                "bug" -> R.color.bug
                "rock" -> R.color.rock
                "ghost" -> R.color.ghost
                "dark" -> R.color.dark
                "dragon" -> R.color.dragon
                "steel" -> R.color.steel
                "fairy" -> R.color.fairy
                else -> R.color.normal
            }
        }
    }
}