package com.platzi.platzidex.util

import android.view.View

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.dissapear() {
    this.visibility = View.INVISIBLE
}