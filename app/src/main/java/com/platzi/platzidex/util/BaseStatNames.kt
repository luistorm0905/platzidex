package com.platzi.platzidex.util

enum class BaseStatNames(val value: String) {
    ATTACK("attack"),
    HP("hp"),
    DEFENSE("defense"),
    SPECIAL_ATTACK("special-attack"),
    SPECIAL_DEFENSE("special-defense"),
    SPEED("speed")
}