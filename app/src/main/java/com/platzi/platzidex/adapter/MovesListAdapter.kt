package com.platzi.platzidex.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.platzi.domain.model.Pokemon
import com.platzi.domain.model.PokemonListItem
import com.platzi.domain.utils.Status
import com.platzi.platzidex.R
import com.platzi.platzidex.databinding.ItemPokemonBinding
import com.platzi.platzidex.util.TypeBackground
import com.platzi.platzidex.util.hide
import com.platzi.platzidex.util.show
import com.platzi.platzidex.viewmodel.PokemonListViewModel
import androidx.palette.graphics.Palette
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.platzi.platzidex.databinding.ItemMoveBinding
import com.platzi.platzidex.mapper.Move


class MovesListAdapter(private val moves: List<Move>
) : RecyclerView.Adapter<MovesListAdapter.MovesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovesViewHolder {
        val binding = ItemMoveBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return MovesViewHolder(binding)

    }

    override fun onBindViewHolder(holder: MovesViewHolder, position: Int) {
        holder.binding.textViewMoveName.text = moves[position].name
    }

    override fun getItemCount() = moves.size

    override fun getItemViewType(position: Int) = position

    inner class MovesViewHolder(val binding: ItemMoveBinding)
        : RecyclerView.ViewHolder(binding.root)
}