package com.platzi.platzidex.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.platzi.platzidex.databinding.ItemFilterBinding
import com.platzi.platzidex.model.FilterType


class TypesAdapter(private val types: List<FilterType>,
                   private val onFilterSelected: (String) -> Unit
) : RecyclerView.Adapter<TypesAdapter.TypesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TypesViewHolder {
        val binding = ItemFilterBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return TypesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TypesViewHolder, position: Int) {
        holder.binding.radioButtonType.text = types[position].name
        holder.binding.radioButtonType.isChecked = types[position].isSelected
        holder.binding.radioButtonType.setOnClickListener {
            clearAllRadioButtons()
            types[position].isSelected = true
            holder.binding.radioButtonType.isChecked = true
            onFilterSelected(types[position].name)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun clearAllRadioButtons() {
        types.forEachIndexed { index, _ ->
            types[index].isSelected = false
        }
        notifyDataSetChanged()
    }

    override fun getItemCount() = types.size

    override fun getItemViewType(position: Int) = position

    inner class TypesViewHolder(val binding: ItemFilterBinding)
        : RecyclerView.ViewHolder(binding.root)
}