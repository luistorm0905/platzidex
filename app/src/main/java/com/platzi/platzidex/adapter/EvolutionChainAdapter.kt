package com.platzi.platzidex.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.platzi.domain.model.Pokemon
import com.platzi.domain.model.PokemonListItem
import com.platzi.domain.utils.Status
import com.platzi.platzidex.R
import com.platzi.platzidex.util.TypeBackground
import com.platzi.platzidex.util.hide
import com.platzi.platzidex.util.show
import com.platzi.platzidex.viewmodel.PokemonListViewModel
import androidx.core.content.ContextCompat
import com.platzi.platzidex.databinding.ItemPokemonInEvolutionChainBinding

class EvolutionChainAdapter(private val pokemonList: List<PokemonListItem>,
                            private val pokemonListViewModel: PokemonListViewModel,
                            private val pokemonClickedListener: (Pokemon) -> Unit
) : RecyclerView.Adapter<EvolutionChainAdapter.EvolutionChainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EvolutionChainViewHolder {
        val binding = ItemPokemonInEvolutionChainBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return EvolutionChainViewHolder(binding)

    }

    override fun onBindViewHolder(holder: EvolutionChainViewHolder, position: Int) {
        pokemonListViewModel.getSpecificPokemonInfo(pokemonList[position].url).observeForever { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    holder.binding.lottieAnimationViewBall.hide()
                    drawPokemonInfo(resource.data ?: Pokemon(), holder)
                }
                Status.ERROR -> {
                    resource.message
                }
                Status.LOADING -> {
                    holder.binding.lottieAnimationViewBall.show()
                }
            }
        }
    }

    private fun drawPokemonInfo(pokemon: Pokemon, holder: EvolutionChainViewHolder) {
        holder.binding.pokemonInfoContainer.show()
        holder.binding.textViewPokemonName.text = holder.itemView.context.getString(
            R.string.pokemon_number_and_name, pokemon.id.toString(), pokemon.name)
        holder.binding.textViewFirstType.text = pokemon.types[0].type.name
        holder.binding.textViewFirstType.backgroundTintList =
            ContextCompat.getColorStateList(holder.itemView.context, TypeBackground.getTypeBackground(pokemon.types[0].type.name))
        if (pokemon.types.size == 2) {
            holder.binding.textViewSecondType.text = pokemon.types[1].type.name
            holder.binding.textViewSecondType.backgroundTintList =
                ContextCompat.getColorStateList(holder.itemView.context, TypeBackground.getTypeBackground(pokemon.types[1].type.name))
        } else {
            holder.binding.textViewSecondType.visibility = View.GONE
        }
        Glide.with(holder.itemView.context)
            .load(pokemon.sprites.otherSprite.officialArtwork.frontDefault)
            .into(holder.binding.imageViewOfficialSprite)
        holder.binding.pokemonInfoContainer.setOnClickListener { pokemonClickedListener(pokemon) }
    }

    override fun getItemCount() = pokemonList.size

    override fun getItemViewType(position: Int) = position

    inner class EvolutionChainViewHolder(val binding: ItemPokemonInEvolutionChainBinding)
        : RecyclerView.ViewHolder(binding.root)
}