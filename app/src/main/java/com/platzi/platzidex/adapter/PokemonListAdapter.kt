package com.platzi.platzidex.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.platzi.domain.model.Pokemon
import com.platzi.domain.model.PokemonListItem
import com.platzi.domain.utils.Status
import com.platzi.platzidex.R
import com.platzi.platzidex.databinding.ItemPokemonBinding
import com.platzi.platzidex.util.TypeBackground
import com.platzi.platzidex.util.hide
import com.platzi.platzidex.util.show
import com.platzi.platzidex.viewmodel.PokemonListViewModel
import androidx.palette.graphics.Palette
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target


class PokemonListAdapter(private val pokemonList: List<PokemonListItem>,
                         private val pokemonListViewModel: PokemonListViewModel,
                         private val pokemonClickedListener: (Pokemon) -> Unit
) : RecyclerView.Adapter<PokemonListAdapter.PokemonListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonListViewHolder {
        val binding = ItemPokemonBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return PokemonListViewHolder(binding)

    }

    override fun onBindViewHolder(holder: PokemonListViewHolder, position: Int) {
        pokemonListViewModel.getSpecificPokemonInfo(pokemonList[position].url).observeForever { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    holder.binding.lottieAnimationViewBall.hide()
                    drawPokemonInfo(resource.data ?: Pokemon(), holder)
                }
                Status.ERROR -> {
                    resource.message
                }
                Status.LOADING -> {
                    holder.binding.lottieAnimationViewBall.show()
                }
            }
        }
    }

    private fun drawPokemonInfo(pokemon: Pokemon, holder: PokemonListViewHolder) {
        holder.binding.textViewPokemonNumberAndName.text = holder.itemView.context.getString(
            R.string.pokemon_number_and_name, pokemon.id.toString(), pokemon.name)
        holder.binding.textViewFirstType.text = pokemon.types[0].type.name
        holder.binding.textViewFirstType.backgroundTintList =
            ContextCompat.getColorStateList(holder.itemView.context, TypeBackground.getTypeBackground(pokemon.types[0].type.name))
        if (pokemon.types.size == 2) {
            holder.binding.textViewSecondType.text = pokemon.types[1].type.name
            holder.binding.textViewSecondType.backgroundTintList =
                ContextCompat.getColorStateList(holder.itemView.context, TypeBackground.getTypeBackground(pokemon.types[1].type.name))
        } else {
            holder.binding.textViewSecondType.visibility = View.GONE
        }
        Glide.with(holder.itemView.context)
            .load(pokemon.sprites.otherSprite.officialArtwork.frontDefault)
            .addListener(object: RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    val palette = Palette.Builder(resource!!.toBitmap())
                        .generate()
                    holder.binding.constraintLayoutBackground.setBackgroundColor(
                        palette.getMutedColor(
                            ContextCompat.getColor(holder.itemView.context, R.color.teal_700)
                        ))
                    return false
                }
            })
            .into(holder.binding.imageViewPokemon)
        holder.binding.cardvViewContainer.setOnClickListener { pokemonClickedListener(pokemon) }
    }

    override fun getItemCount() = pokemonList.size

    override fun getItemViewType(position: Int) = position

    inner class PokemonListViewHolder(val binding: ItemPokemonBinding)
        : RecyclerView.ViewHolder(binding.root)
}