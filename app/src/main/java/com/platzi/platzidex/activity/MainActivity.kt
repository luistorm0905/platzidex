package com.platzi.platzidex.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.platzi.platzidex.databinding.ActivityMainBinding
import com.platzi.platzidex.R
import com.platzi.platzidex.fragment.PokemonListFragment


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        goToPokemonListFragment()
    }

    private fun goToPokemonListFragment() {
        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, PokemonListFragment.newInstance())
            .commit()
    }
}