package com.platzi.platzidex.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.platzi.domain.model.Pokemon
import com.platzi.domain.model.PokemonListItem
import com.platzi.domain.utils.Status
import com.platzi.platzidex.R
import com.platzi.platzidex.adapter.PokemonListAdapter
import com.platzi.platzidex.databinding.FragmentPokemonListBinding
import com.platzi.platzidex.util.hide
import com.platzi.platzidex.util.show
import com.platzi.platzidex.viewmodel.PokemonListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class PokemonListFragment : BaseFragment() {

    private var _binding: FragmentPokemonListBinding? = null
    private val binding: FragmentPokemonListBinding get() = _binding!!

    private val pokemonListViewModel: PokemonListViewModel by viewModel()

    private lateinit var fullList: List<PokemonListItem>
    private lateinit var actualList: List<PokemonListItem>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPokemonListBinding.inflate(LayoutInflater.from(requireContext()), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadPokemonFullList()
        setListeners()
    }

    private fun setListeners() {
        binding.searchView.setOnFilterSelected {
            if (it.isNullOrEmpty()) {
                drawPokemonList(fullList)
            } else {
                changeToFilteredPokemon(it)
            }
        }
        binding.searchView.setOnSearchListener {
            if (it.isNullOrEmpty()) {
                actualList = fullList
                drawPokemonList(actualList)
            } else {
                searchInList(it)
            }
        }
    }

    private fun searchInList(query: String) {
        val items = actualList.filter {
            it.name.lowercase().contains(query)
        }
        drawPokemonList(items)
    }

    private fun changeToFilteredPokemon(filter: String) {
        pokemonListViewModel.getFilteredList(filter).observe(viewLifecycleOwner, { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    resource.data?.let { typeFilterResponse ->
                        actualList = typeFilterResponse.pokemon.map {
                            it.pokemonListItem
                        }
                        drawPokemonList(actualList)
                    }
                    hideLoader()
                }
                Status.ERROR -> {
                    hideLoader()
                    showError(resource.message.orEmpty())
                }
                Status.LOADING -> {
                    showLoader()
                }
            }
        })
    }

    private fun loadPokemonFullList() {
        pokemonListViewModel.getPokemonList().observe(viewLifecycleOwner, { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    resource.data?.let {
                        fullList = it.results
                        drawPokemonList(fullList)
                        actualList = fullList
                    }
                    hideLoader()
                }
                Status.ERROR -> {
                    hideLoader()
                    showError(resource.message.orEmpty())
                }
                Status.LOADING -> {
                    showLoader()
                }
            }
        })
    }

    private fun showLoader() {
        binding.loaderView.show()
        binding.loaderView.bringToFront()
    }

    private fun hideLoader() {
        binding.loaderView.hide()
    }

    private fun drawPokemonList(list: List<PokemonListItem>) {
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewPokemonList.layoutManager = layoutManager
        binding.recyclerViewPokemonList.adapter = PokemonListAdapter(
            list, pokemonListViewModel, ::goToPokemonDetailFragment)
    }

    private fun goToPokemonDetailFragment(pokemon: Pokemon) {
        parentFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, PokemonDetailFragment.newInstance(pokemon))
            .addToBackStack(null)
            .commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() = PokemonListFragment()
    }
}