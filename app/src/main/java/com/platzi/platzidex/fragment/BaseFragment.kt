package com.platzi.platzidex.fragment

import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.platzi.platzidex.R

open class BaseFragment: Fragment() {

    protected fun showError(errorMessage: String) {
        view?.let {
            Snackbar
                .make(it.rootView, errorMessage, Snackbar.LENGTH_LONG)
                .setBackgroundTint(ResourcesCompat.getColor(resources, R.color.purple_200, null))
                .show()
        }
    }

    protected fun showInfoMessage(errorMessage: String) {
        view?.let {
            Snackbar
                .make(it.rootView, errorMessage, Snackbar.LENGTH_LONG)
                .setBackgroundTint(ResourcesCompat.getColor(resources, R.color.teal_200, null))
                .show()
        }
    }

}