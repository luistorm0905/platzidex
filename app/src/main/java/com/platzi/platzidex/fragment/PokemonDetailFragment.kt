package com.platzi.platzidex.fragment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.platzi.domain.utils.Status
import com.platzi.platzidex.R
import com.platzi.platzidex.databinding.FragmentPokemonDetailBinding
import com.platzi.platzidex.mapper.Pokemon
import com.platzi.platzidex.mapper.PokemonMapper
import com.platzi.platzidex.util.*
import com.platzi.platzidex.viewmodel.PokemonDetailViewModel
import com.platzi.platzidex.viewmodel.PokemonListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class PokemonDetailFragment : BaseFragment() {

    private var _binding: FragmentPokemonDetailBinding? = null
    private val binding: FragmentPokemonDetailBinding get() = _binding!!

    private val pokemonDetailViewModel: PokemonDetailViewModel by viewModel()
    private val pokemonListViewModel: PokemonListViewModel by viewModel()

    private lateinit var pokemon: Pokemon

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPokemonDetailBinding.inflate(LayoutInflater.from(requireContext()), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            pokemon = it.getParcelable(POKEMON) ?: Pokemon()
            initViews()
        }
    }

    private fun initViews() {
        binding.textViewPokemonName.text = pokemon.name
        binding.textViewFirstType.text = pokemon.types[0].type.name
        binding.textViewFirstType.backgroundTintList =
            ContextCompat.getColorStateList(requireContext(),
                TypeBackground.getTypeBackground(pokemon.types[0].type.name))
        if (pokemon.types.size == 2) {
            binding.textViewSecondType.text = pokemon.types[1].type.name
            binding.textViewSecondType.backgroundTintList =
                ContextCompat.getColorStateList(requireContext(),
                    TypeBackground.getTypeBackground(pokemon.types[1].type.name))
        } else {
            binding.textViewSecondType.visibility = View.GONE
        }
        Glide
            .with(requireContext())
            .load(pokemon.sprites.otherSprite.officialArtwork.frontDefault)
            .addListener(object: RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    val palette = Palette.Builder(resource!!.toBitmap())
                        .generate()
                    binding.constraintLayoutBackground.setBackgroundColor(
                        palette.getMutedColor(
                            ContextCompat.getColor(requireContext(), R.color.teal_700)
                        ))
                    return false
                }
            })
            .into(binding.imageViewOfficialSprite)
        Glide
            .with(requireContext())
            .load(pokemon.sprites.frontDefault)
            .into(binding.imageViewNormal)
        Glide
            .with(requireContext())
            .load(pokemon.sprites.frontShiny)
            .into(binding.imageViewShiny)
        binding.basicPokemonInfoView.setBasicInfo(pokemon)
        binding.statsInfoView.setStatsInfo(pokemon.stats)
        getSpeciesInfo()
        getAbilitiesInfo()
        binding.movesView.drawMoves(pokemon.moves)
    }

    private fun getSpeciesInfo() {
        pokemonDetailViewModel.getSpeciesInfo(pokemon.species.url).observe(viewLifecycleOwner, { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    resource.data?.let {
                        binding.basicPokemonInfoView.drawSpeciesInfo(it)
                        getEvolutionChain(it.evolutionChain.url)
                    }
                }
                Status.ERROR -> {
                    binding.basicPokemonInfoView.hideLoader()
                    showError(resource.message.orEmpty())
                }
                Status.LOADING -> {
                    binding.basicPokemonInfoView.showLoader()
                }
            }
        })
    }

    private fun getEvolutionChain(url: String) {
        pokemonDetailViewModel.getEvolutionChain(url).observe(viewLifecycleOwner, { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    resource.data?.let {
                        binding.evolutionChainView.drawEvolutionChain(it, pokemonListViewModel, ::goToPokemonDetailFragment)
                    }
                }
                Status.ERROR -> {
                    showError(resource.message.orEmpty())
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun goToPokemonDetailFragment(pokemon: com.platzi.domain.model.Pokemon) {
        parentFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, newInstance(pokemon))
            .addToBackStack(null)
            .commit()
    }

    private fun getAbilitiesInfo() {
        pokemon.abilities.forEachIndexed { index, abilityInfo ->
            pokemonDetailViewModel.getAbilityInfo(abilityInfo.ability.url, index)
                .observe(viewLifecycleOwner, { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            resource.data?.let {
                                binding.abilitiesInfoView.drawAbility(it, pokemon.abilities.size)
                            }
                        }
                        Status.ERROR -> {
                            binding.abilitiesInfoView.hideLoader()
                            showError(resource.message.orEmpty())
                        }
                        Status.LOADING -> {
                            binding.abilitiesInfoView.showLoader()
                        }
                    }
                })
        }
    }

    companion object {

        private const val POKEMON = "POKEMON"

        @JvmStatic
        fun newInstance(pokemon: com.platzi.domain.model.Pokemon) = PokemonDetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(POKEMON, PokemonMapper.map(pokemon))
            }
        }
    }
}