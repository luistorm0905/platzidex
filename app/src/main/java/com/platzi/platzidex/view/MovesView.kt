package com.platzi.platzidex.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.platzi.platzidex.adapter.MovesListAdapter
import com.platzi.platzidex.databinding.ViewMovesBinding
import com.platzi.platzidex.mapper.Move

class MovesView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    private val binding = ViewMovesBinding
        .inflate(LayoutInflater.from(context), this, true)

    init {
        binding.imageViewHide.setOnClickListener {
            binding.recyclerViewMoves.isVisible = !binding.recyclerViewMoves.isVisible
        }
    }

    fun drawMoves(moves: List<Move>) {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewMoves.layoutManager = layoutManager
        binding.recyclerViewMoves.adapter = MovesListAdapter(moves)
    }
}