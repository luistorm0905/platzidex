package com.platzi.platzidex.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.platzi.domain.model.AbilityResponse
import com.platzi.platzidex.R
import com.platzi.platzidex.databinding.ViewAbilitiesInfoBinding
import com.platzi.platzidex.util.PokeApiLanguages
import com.platzi.platzidex.util.hide
import com.platzi.platzidex.util.show
import java.util.Locale

private const val FULL_ABILITIES = 3

class AbilitiesInfoView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    private val binding = ViewAbilitiesInfoBinding
        .inflate(LayoutInflater.from(context), this, true)

    fun drawAbility(abilityResponse: AbilityResponse, totalAbilities: Int) {
        binding.lottieAnimationViewBallAbilities.hide()
        val abilityDescription = if (Locale.getDefault().language == PokeApiLanguages.SPANISH.value) {
            abilityResponse.flavorTextEntries.first {
                it.language.name == PokeApiLanguages.SPANISH.value
            }.flavorText
        } else {
            abilityResponse.flavorTextEntries.first {
                it.language.name == PokeApiLanguages.ENGLISH.value
            }.flavorText
        }
        if (totalAbilities == FULL_ABILITIES) {
            when(abilityResponse.index) {
                0 -> {
                    binding.textViewFirstAbility.text =
                        context.getString(R.string.text_ability_description, abilityResponse.name, abilityDescription)
                    binding.textViewFirstAbility.show()
                }
                1 -> {
                    binding.textViewSecondAbility.text = context.getString(R.string.text_ability_description, abilityResponse.name, abilityDescription)
                    binding.textViewSecondAbility.show()
                }
                else -> {
                    binding.textViewHiddenAbility.text =
                        context.getString(R.string.text_ability_description, abilityResponse.name, abilityDescription)
                    binding.textViewHiddenAbility.show()
                }
            }
        } else {
            when(abilityResponse.index) {
                0 -> {
                    binding.textViewFirstAbility.text =
                        context.getString(R.string.text_ability_description, abilityResponse.name, abilityDescription)
                    binding.textViewFirstAbility.show()
                }
                else -> {
                    binding.textViewHiddenAbility.text =
                        context.getString(
                            R.string.text_ability_description,
                            abilityResponse.name,
                            abilityDescription
                        )
                    binding.textViewHiddenAbility.show()

                }
            }
        }
    }

    fun showLoader() {
        binding.lottieAnimationViewBallAbilities.show()
    }

    fun hideLoader() {
        binding.lottieAnimationViewBallAbilities.hide()
    }
}