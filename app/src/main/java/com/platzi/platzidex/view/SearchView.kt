package com.platzi.platzidex.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.platzi.domain.model.SpeciesResponse
import com.platzi.platzidex.R
import com.platzi.platzidex.adapter.MovesListAdapter
import com.platzi.platzidex.adapter.TypesAdapter
import com.platzi.platzidex.databinding.ViewBasicPokemonInfoBinding
import com.platzi.platzidex.databinding.ViewSearchBinding
import com.platzi.platzidex.databinding.ViewStatsInfoBinding
import com.platzi.platzidex.mapper.BaseStat
import com.platzi.platzidex.mapper.Pokemon
import com.platzi.platzidex.model.FilterType
import com.platzi.platzidex.util.*
import java.util.Locale

class SearchView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    private val binding = ViewSearchBinding
        .inflate(LayoutInflater.from(context), this, true)

    private lateinit var onFilterSelectedListener: (String) -> Unit
    private lateinit var onSearchListener: (String) -> Unit

    init {
        val layoutManager = GridLayoutManager(context, 3,LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewFilters.layoutManager = layoutManager
        binding.recyclerViewFilters.adapter = TypesAdapter(
            mutableListOf<FilterType>().apply{
                resources.getStringArray(R.array.pokemon_types).forEach {
                    this.add(FilterType(it))
                }
            }
        ) {
          onFilterSelectedListener(it)
        }
        binding.imageViewFilter.setOnClickListener {
            binding.recyclerViewFilters.isVisible = ! binding.recyclerViewFilters.isVisible
            if (!binding.recyclerViewFilters.isVisible) {
                onFilterSelectedListener("")
            }
        }
        binding.editTextSearch.addTextChangedListener {
            onSearchListener(it.toString())
        }
    }

    fun setOnFilterSelected(onFilterSelectedListener: (String) -> Unit) {
        this.onFilterSelectedListener = onFilterSelectedListener
    }

    fun setOnSearchListener(onSearchListener: (String) -> Unit) {
        this.onSearchListener = onSearchListener
    }
}