package com.platzi.platzidex.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.platzi.domain.model.SpeciesResponse
import com.platzi.platzidex.R
import com.platzi.platzidex.databinding.ViewBasicPokemonInfoBinding
import com.platzi.platzidex.mapper.Pokemon
import com.platzi.platzidex.util.PokeApiLanguages
import com.platzi.platzidex.util.hide
import com.platzi.platzidex.util.moveAUnit
import com.platzi.platzidex.util.show
import java.util.Locale

class BasicPokemonInfoView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    private val binding = ViewBasicPokemonInfoBinding
        .inflate(LayoutInflater.from(context), this, true)

    fun setBasicInfo(pokemon: Pokemon) {
        binding.textViewNationalNumber.text = context.getString(R.string.text_national_dex_number, pokemon.id.toString())
        binding.textViewHeight.text = context.getString(R.string.text_height, pokemon.height.moveAUnit().toString())
        binding.textViewWeight.text = context.getString(R.string.text_weight, pokemon.weight.moveAUnit().toString())
    }

    fun showLoader() {
        binding.lottieAnimationViewBall.show()
        binding.textViewPokedexDescription.hide()
    }

    fun hideLoader() {
        binding.lottieAnimationViewBall.hide()
    }

    fun drawSpeciesInfo(speciesResponse: SpeciesResponse) {
        if (Locale.getDefault().language == PokeApiLanguages.SPANISH.value) {
            binding.textViewPokedexDescription.text =
                speciesResponse.flavorTextEntries.first {
                    it.language.name == PokeApiLanguages.SPANISH.value
                }.flavorText
        } else {
            binding.textViewPokedexDescription.text =
                speciesResponse.flavorTextEntries.first {
                    it.language.name == PokeApiLanguages.ENGLISH.value
                }.flavorText
        }
        binding.textViewPokedexDescription.show()
        binding.lottieAnimationViewBall.hide()
    }
}