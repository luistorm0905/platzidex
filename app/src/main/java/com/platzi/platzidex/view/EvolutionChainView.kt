package com.platzi.platzidex.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.platzi.domain.model.EvolutionChainResponse
import com.platzi.domain.model.EvolvesTo
import com.platzi.domain.model.PokemonListItem
import com.platzi.domain.model.SpeciesResponse
import com.platzi.platzidex.R
import com.platzi.platzidex.adapter.EvolutionChainAdapter
import com.platzi.platzidex.adapter.PokemonListAdapter
import com.platzi.platzidex.databinding.ViewBasicPokemonInfoBinding
import com.platzi.platzidex.databinding.ViewEvolutionChainBinding
import com.platzi.platzidex.databinding.ViewStatsInfoBinding
import com.platzi.platzidex.mapper.BaseStat
import com.platzi.platzidex.mapper.Pokemon
import com.platzi.platzidex.util.*
import com.platzi.platzidex.viewmodel.PokemonListViewModel
import java.util.Locale

private const val SPECIES_URL = "-species"

class EvolutionChainView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    private val binding = ViewEvolutionChainBinding
        .inflate(LayoutInflater.from(context), this, true)
    private var pokemonList = mutableListOf<PokemonListItem>()

    fun drawEvolutionChain(evolutionChainResponse: EvolutionChainResponse,
                           pokemonListViewModel: PokemonListViewModel,
                           pokemonClickedListener: (com.platzi.domain.model.Pokemon) -> Unit) {
        pokemonList.add(
            PokemonListItem(
                name = evolutionChainResponse.chain.species.name,
                url = evolutionChainResponse.chain.species.url.replace(SPECIES_URL, "")
        ))
        evolutionChainResponse.chain.evolvesTo.forEach {
            getEvolutionToList(it)
        }
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerViewEvolutionChain.layoutManager = layoutManager
        binding.recyclerViewEvolutionChain.adapter = EvolutionChainAdapter(
            pokemonList, pokemonListViewModel, pokemonClickedListener)
    }

    private fun getEvolutionToList(evolvesTo: EvolvesTo) {
        if (evolvesTo.evolvesTo.isEmpty()) {
            pokemonList.add(
                PokemonListItem(
                    name = evolvesTo.species.name,
                    url = evolvesTo.species.url.replace(SPECIES_URL, "")
            ))
        } else {
            pokemonList.add(
                PokemonListItem(
                    name = evolvesTo.species.name,
                    url = evolvesTo.species.url.replace(SPECIES_URL, "")
                ))
            evolvesTo.evolvesTo.forEach {
                getEvolutionToList(it)
            }
        }
    }
}