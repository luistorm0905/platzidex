package com.platzi.platzidex.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.platzi.domain.model.SpeciesResponse
import com.platzi.platzidex.R
import com.platzi.platzidex.databinding.ViewBasicPokemonInfoBinding
import com.platzi.platzidex.databinding.ViewStatsInfoBinding
import com.platzi.platzidex.mapper.BaseStat
import com.platzi.platzidex.mapper.Pokemon
import com.platzi.platzidex.util.*
import java.util.Locale

class StatsInfoView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    private val binding = ViewStatsInfoBinding
        .inflate(LayoutInflater.from(context), this, true)

    fun setStatsInfo(stats: List<BaseStat>) {
        val hp = stats.first {
            it.stat.name == BaseStatNames.HP.value
        }.baseStatValue
        binding.progressBarHP.progress = hp
        binding.textViewHPValue.text = hp.toString()
        val attack = stats.first {
            it.stat.name == BaseStatNames.ATTACK.value
        }.baseStatValue
        binding.progressBarAttack.progress = attack
        binding.textViewAttackValue.text = attack.toString()
        val defense = stats.first {
            it.stat.name == BaseStatNames.DEFENSE.value
        }.baseStatValue
        binding.progressBarDefense.progress = defense
        binding.textViewDefenseValue.text = defense.toString()
        val specialAttack = stats.first {
            it.stat.name == BaseStatNames.SPECIAL_ATTACK.value
        }.baseStatValue
        binding.progressBarSpecialAttack.progress = specialAttack
        binding.textViewSpecialAttackValue.text = specialAttack.toString()
        val specialDefense = stats.first {
            it.stat.name == BaseStatNames.SPECIAL_DEFENSE.value
        }.baseStatValue
        binding.progressBarSpecialDefense.progress = specialDefense
        binding.textViewSpecialDefenseValue.text = specialDefense.toString()
        val speed = stats.first {
            it.stat.name == BaseStatNames.SPEED.value
        }.baseStatValue
        binding.progressBarSpeed.progress = speed
        binding.textViewSpeedValue.text = speed.toString()
    }
}