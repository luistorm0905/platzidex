package com.platzi.platzidex.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.platzi.platzidex.databinding.ViewRotomLoaderBinding

class RotomLoaderView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    private val binding = ViewRotomLoaderBinding
        .inflate(LayoutInflater.from(context), this, true)
}