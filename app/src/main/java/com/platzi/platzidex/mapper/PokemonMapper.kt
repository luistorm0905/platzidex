package com.platzi.platzidex.mapper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Pokemon(
    val id: Int = 0,
    val name: String = "",
    val height: Double = 0.0,
    val weight: Double = 0.0,
    val types: List<PokemonType> = listOf(),
    val sprites: Sprites = Sprites(),
    val stats: List<BaseStat> = listOf(),
    val abilities: List<AbilityInfo> = listOf(),
    val species: Species = Species(),
    val moves: List<Move> = listOf()
): Parcelable

@Parcelize
data class PokemonType(
    val slot: Int = 0,
    val type: Type = Type()
): Parcelable

@Parcelize
data class Type(
    val name: String = ""
): Parcelable

@Parcelize
data class Sprites(
    val frontDefault: String = "",
    val frontShiny: String = "",
    val otherSprite: OtherSprite = OtherSprite()
): Parcelable

@Parcelize
data class OtherSprite(
    val officialArtwork: OfficialArtwork = OfficialArtwork()
): Parcelable

@Parcelize
data class OfficialArtwork(
    val frontDefault: String = ""
): Parcelable

@Parcelize
data class BaseStat(
    val baseStatValue: Int = 0,
    val stat: Stat = Stat()
): Parcelable

@Parcelize
data class Stat(
    val name: String = ""
): Parcelable

@Parcelize
data class AbilityInfo(
    val ability: Ability = Ability(),
    val isHidden: Boolean = false
): Parcelable

@Parcelize
data class Ability(
    val name: String = "",
    val url: String = ""
): Parcelable

@Parcelize
data class Species(
    val name: String = "",
    val url: String = ""
): Parcelable

@Parcelize
data class Move(
    val name: String = "",
): Parcelable

object PokemonMapper {
    fun map(pokemon: com.platzi.domain.model.Pokemon): Pokemon {
        return Pokemon(
            id = pokemon.id,
            name = pokemon.name,
            height = pokemon.height,
            weight = pokemon.weight,
            types = mutableListOf<PokemonType>().apply {
                pokemon.types.forEach {
                    this.add(
                        PokemonType(
                            it.slot,
                            Type(it.type.name)
                        )
                    )
                }
            },
            sprites = Sprites(
                frontDefault = pokemon.sprites.frontDefault,
                frontShiny = pokemon.sprites.frontShiny,
                otherSprite = OtherSprite(OfficialArtwork(pokemon.sprites.otherSprite.officialArtwork.frontDefault))
            ),
            stats = mutableListOf<BaseStat>().apply {
                pokemon.stats.forEach {
                    this.add(
                        BaseStat(
                            it.baseStatValue,
                            Stat(it.stat.name)
                        )
                    )
                }
            },
            abilities = mutableListOf<AbilityInfo>().apply {
                pokemon.abilities.forEach {
                    this.add(
                        AbilityInfo(
                            Ability(it.ability.name, it.ability.url),
                            it.isHidden
                        )
                    )
                }
            },
            species = Species(
                name = pokemon.species.name,
                url = pokemon.species.url
            ),
            moves = mutableListOf<Move>().apply {
                pokemon.moves.forEach {
                    this.add(Move(it.moveInfo.name))
                }
            }
        )
    }
}