package com.platzi.platzidex.di

import com.platzi.data.api.PokeApiHelper
import com.platzi.data.repositoryimpl.PokeApiRepositoryImpl
import com.platzi.domain.repository.PokeApiRepository
import com.platzi.platzidex.viewmodel.PokemonListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val PokeListModule = module {
    factory<PokeApiRepository> { PokeApiRepositoryImpl(get(), get())}
    factory { PokeApiHelper(get()) }
    viewModel { PokemonListViewModel(get()) }
}