package com.platzi.platzidex.di

import com.platzi.platzidex.viewmodel.PokemonDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val PokeDetailModule = module {
    viewModel { PokemonDetailViewModel(get()) }
}