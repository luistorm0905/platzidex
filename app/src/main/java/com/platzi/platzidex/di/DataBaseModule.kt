package com.platzi.platzidex.di

import androidx.room.Room
import com.platzi.data.database.PlatziDexDatabase
import com.platzi.data.database.PokemonListItemDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val DataBaseModule = module {
    factory<PokemonListItemDao> {
        Room.databaseBuilder(
            androidApplication().baseContext,
            PlatziDexDatabase::class.java, "platzidex-db"
        ).build().pokemonListItemDao()
    }
}