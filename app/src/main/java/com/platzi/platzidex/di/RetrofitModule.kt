package com.platzi.platzidex.di

import com.platzi.data.api.PokeApiService
import com.platzi.data.api.RetrofitBuilder
import org.koin.dsl.module

val RetrofitModule = module {
    single {
        RetrofitBuilder.createServiceApi(
           PokeApiService::class.java
        )
    }
}