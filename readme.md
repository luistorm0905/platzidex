# This is the PlatziDex!

Hi, my name is Luis Torres and this is my android developer test for Platzi.

Here you can see an android app with Clean Architecture, MVVM, Koin and Retrofit, so let me explain it (I used https://medium.com/android-dev-hacks/detailed-guide-on-android-clean-architecture-9eab262a9011 to guide and other sources):

1. In the domain layer you will see the models, the repositories and some utils. The models are the return types of the repository and the repository is the interface that calls the needed method for get data.

2. In the data layer you will find the API and Database related things, as well you'll find the implementation of the repository who decides whats to do with the data.

3. The app layer (presentation) has all the UI of the app.

I know the app is far from perfect, but I put on it a lot of love, so enjoy it :)