package com.platzi.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.platzi.data.model.PokemonListItemEntity

@Dao
interface PokemonListItemDao {

    @Insert
    fun insert(pokemonListItems: PokemonListItemEntity)

    @Query("SELECT * FROM pokemonListItems WHERE name=:pokemonName")
    fun findByName(pokemonName: String): PokemonListItemEntity?
}