package com.platzi.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.platzi.data.model.PokemonListItemEntity

@Database(entities = [PokemonListItemEntity::class], version = 1)
abstract class PlatziDexDatabase: RoomDatabase() {
    abstract fun pokemonListItemDao() : PokemonListItemDao
}