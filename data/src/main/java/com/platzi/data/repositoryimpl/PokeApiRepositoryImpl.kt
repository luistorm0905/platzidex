package com.platzi.data.repositoryimpl

import com.platzi.data.api.PokeApiHelper
import com.platzi.data.database.PokemonListItemDao
import com.platzi.data.model.PokemonListItemEntity
import com.platzi.domain.repository.PokeApiRepository

class PokeApiRepositoryImpl(private val pokeApiHelper: PokeApiHelper,
                            private val pokemonListItemDao: PokemonListItemDao) : PokeApiRepository {

    override suspend fun getPokemonList() = pokeApiHelper.getPokemonList().also {
        it.results.forEach { pokemonListItem ->
            val pokemon: PokemonListItemEntity? = pokemonListItemDao.findByName(pokemonListItem.name)
            if (pokemon == null) {
                pokemonListItemDao.insert(PokemonListItemEntity(pokemonListItem.name))
            }
        }
    }

    override suspend fun getSpecificPokemonInfo(url: String) = pokeApiHelper.getSpecificPokemonInfo(url)

    override suspend fun getSpeciesInfo(url: String) = pokeApiHelper.getSpeciesInfo(url)

    override suspend fun getAbilitiesInfo(url: String) = pokeApiHelper.getAbilitiesInfo(url)

    override suspend fun getEvolutionChain(url: String) = pokeApiHelper.getEvolutionChain(url)

    override suspend fun getPokemonByType(typeName: String) = pokeApiHelper.getPokemonByType(typeName)

}