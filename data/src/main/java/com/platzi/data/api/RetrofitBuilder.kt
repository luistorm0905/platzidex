package com.platzi.data.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://pokeapi.co/api/v2/"

class RetrofitBuilder {

    companion object {
        private fun getRetrofit(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        fun <S> createServiceApi(
            serviceClass: Class<S>
        ): S {
            return getRetrofit().create(serviceClass)
        }
    }


}