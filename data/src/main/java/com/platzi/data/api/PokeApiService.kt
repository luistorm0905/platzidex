package com.platzi.data.api

import com.platzi.domain.model.*
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

private const val POKEMON_LIST_URL = "pokemon"
private const val POKEMON_TYPE_URL = "type/{name}"
private const val POKEMON_LIST_LIMIT_PARAM = "limit"
private const val POKEMON_TYPE_PARAM = "name"

interface PokeApiService {

    @GET(POKEMON_LIST_URL)
    suspend fun getPokemonList(@Query(POKEMON_LIST_LIMIT_PARAM) limit: Int): PokemonListResponse

    @GET
    suspend fun getSpecificPokemonInfo(@Url url: String): Pokemon

    @GET
    suspend fun getSpeciesInfo(@Url url: String): SpeciesResponse

    @GET
    suspend fun getAbilitiesInfo(@Url url: String): AbilityResponse

    @GET
    suspend fun getEvolutionChain(@Url url: String): EvolutionChainResponse

    @GET(POKEMON_TYPE_URL)
    suspend fun getPokemonFilteredByType(@Path(POKEMON_TYPE_PARAM) typeName: String): TypeFilterResponse
}