package com.platzi.data.api

class PokeApiHelper(private val pokeApiService: PokeApiService) {

    suspend fun getPokemonList() = pokeApiService.getPokemonList(LIMIT)

    suspend fun getSpecificPokemonInfo(url: String) = pokeApiService.getSpecificPokemonInfo(url)

    suspend fun getSpeciesInfo(url: String) = pokeApiService.getSpeciesInfo(url)

    suspend fun getAbilitiesInfo(url: String) = pokeApiService.getAbilitiesInfo(url)

    suspend fun getEvolutionChain(url: String) = pokeApiService.getEvolutionChain(url)

    suspend fun getPokemonByType(typeName: String) = pokeApiService.getPokemonFilteredByType(typeName.lowercase())

    companion object {
        private const val LIMIT = 898
    }

}