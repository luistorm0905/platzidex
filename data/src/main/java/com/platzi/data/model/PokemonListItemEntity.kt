package com.platzi.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemonListItems")
data class PokemonListItemEntity(
    @PrimaryKey @ColumnInfo(name = "name") val name: String = ""
)
