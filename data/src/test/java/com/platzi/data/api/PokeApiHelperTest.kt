package com.platzi.data.api

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class PokeApiHelperTest {

    @Mock
    private lateinit var pokeApiService: PokeApiService

    private lateinit var pokeApiHelper: PokeApiHelper

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        pokeApiHelper = PokeApiHelper(pokeApiService)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `when get pokemon list, then call the correct service`() {
        runBlockingTest {
            pokeApiHelper.getPokemonList()
            Mockito.verify(pokeApiService).getPokemonList(898)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `when get specific pokemon info, then call the correct service`() {
        runBlockingTest {
            pokeApiHelper.getSpecificPokemonInfo("test")
            Mockito.verify(pokeApiService).getSpecificPokemonInfo("test")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `when get species info, then call the correct service`() {
        runBlockingTest {
            pokeApiHelper.getSpeciesInfo("test")
            Mockito.verify(pokeApiService).getSpeciesInfo("test")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `when get abilities info, then call the correct service`() {
        runBlockingTest {
            pokeApiHelper.getAbilitiesInfo("test")
            Mockito.verify(pokeApiService).getAbilitiesInfo("test")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `when get evolution chain, then call the correct service`() {
        runBlockingTest {
            pokeApiHelper.getEvolutionChain("test")
            Mockito.verify(pokeApiService).getEvolutionChain("test")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `when get pokemon by type, then call the correct service`() {
        runBlockingTest {
            pokeApiHelper.getPokemonByType("teSt")
            Mockito.verify(pokeApiService).getPokemonFilteredByType("test")
        }
    }

}