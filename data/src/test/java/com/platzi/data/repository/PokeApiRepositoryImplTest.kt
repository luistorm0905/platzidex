package com.platzi.data.repository

import com.platzi.data.api.PokeApiHelper
import com.platzi.data.database.PokemonListItemDao
import com.platzi.data.model.PokemonListItemEntity
import com.platzi.data.repositoryimpl.PokeApiRepositoryImpl
import com.platzi.domain.model.PokemonListItem
import com.platzi.domain.model.PokemonListResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class PokeApiRepositoryImplTest {
    @Mock
    private lateinit var pokeApiHelper: PokeApiHelper

    @Mock
    private lateinit var pokemonListItemDao: PokemonListItemDao

    private lateinit var pokeApiRepositoryImpl: PokeApiRepositoryImpl

    @Before
    fun setupVariables() {
        MockitoAnnotations.openMocks(this)
        pokeApiRepositoryImpl = PokeApiRepositoryImpl(pokeApiHelper, pokemonListItemDao)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `When get pokemon list, then call the correct helper method and save in DB`() {
        runBlockingTest {
        `when`(pokeApiHelper.getPokemonList()).thenReturn(PokemonListResponse(
            results = listOf(PokemonListItem("test", "test"))
        ))
            pokeApiRepositoryImpl.getPokemonList()
            verify(pokeApiHelper).getPokemonList()
            verify(pokemonListItemDao).insert(PokemonListItemEntity("test"))
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `When get specific pokemon info, then call the correct helper method`() {
        runBlockingTest {
            pokeApiRepositoryImpl.getSpecificPokemonInfo("test")
            verify(pokeApiHelper).getSpecificPokemonInfo("test")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `When get species info, then call the correct helper method`() {
        runBlockingTest {
            pokeApiRepositoryImpl.getSpeciesInfo("test")
            verify(pokeApiHelper).getSpeciesInfo("test")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `When get abilities info, then call the correct helper method`() {
        runBlockingTest {
            pokeApiRepositoryImpl.getAbilitiesInfo("test")
            verify(pokeApiHelper).getAbilitiesInfo("test")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `When get evolution chain, then call the correct helper method`() {
        runBlockingTest {
            pokeApiRepositoryImpl.getEvolutionChain("test")
            verify(pokeApiHelper).getEvolutionChain("test")
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `When get pokemon by type, then call the correct helper method`() {
        runBlockingTest {
            pokeApiRepositoryImpl.getPokemonByType("test")
            verify(pokeApiHelper).getPokemonByType("test")
        }
    }
}