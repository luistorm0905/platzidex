package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class BaseStat(
    @SerializedName("base_stat")
    val baseStatValue: Int = 0,
    @SerializedName("stat")
    val stat: Stat = Stat()
)
