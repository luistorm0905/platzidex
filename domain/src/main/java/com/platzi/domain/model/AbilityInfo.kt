package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class AbilityInfo(
    @SerializedName("ability")
    val ability: Ability = Ability(),
    @SerializedName("is_hidden")
    val isHidden: Boolean = false
)
