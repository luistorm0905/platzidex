package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class SpeciesResponse(
    @SerializedName("flavor_text_entries")
    val flavorTextEntries: List<FlavorTextEntry> = listOf(),
    @SerializedName("evolution_chain")
    val evolutionChain: EvolutionChain = EvolutionChain()
)