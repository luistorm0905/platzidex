package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class PokemonType(
    @SerializedName("slot")
    val slot: Int = 0,
    @SerializedName("type")
    val type: Type = Type()
)
