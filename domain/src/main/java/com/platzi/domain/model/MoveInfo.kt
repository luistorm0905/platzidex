package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class MoveInfo(
    @SerializedName("name")
    val name: String = ""
)
