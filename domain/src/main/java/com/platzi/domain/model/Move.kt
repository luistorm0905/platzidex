package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class Move(
    @SerializedName("move")
    val moveInfo: MoveInfo = MoveInfo()
)
