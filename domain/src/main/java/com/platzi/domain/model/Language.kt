package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class Language(
    @SerializedName("name")
    val name: String = ""
)
