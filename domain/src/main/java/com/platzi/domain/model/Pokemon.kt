package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class Pokemon(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("height")
    val height: Double = 0.0,
    @SerializedName("weight")
    val weight: Double = 0.0,
    @SerializedName("types")
    val types: List<PokemonType> = listOf(),
    @SerializedName("sprites")
    val sprites: Sprites = Sprites(),
    @SerializedName("stats")
    val stats: List<BaseStat> = listOf(),
    @SerializedName("abilities")
    val abilities: List<AbilityInfo> = listOf(),
    @SerializedName("species")
    val species: Species = Species(),
    @SerializedName("moves")
    val moves: List<Move> = listOf()
)