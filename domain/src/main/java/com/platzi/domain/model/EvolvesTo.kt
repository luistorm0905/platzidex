package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class EvolvesTo(
    @SerializedName("evolves_to")
    val evolvesTo: List<EvolvesTo> = listOf(),
    @SerializedName("species")
    val species: Species = Species()
)
