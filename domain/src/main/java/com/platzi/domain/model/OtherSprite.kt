package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class OtherSprite(
    @SerializedName("official-artwork")
    val officialArtwork: OfficialArtwork = OfficialArtwork()
)
