package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class FlavorTextEntry(
    @SerializedName("flavor_text")
    val flavorText: String = "",
    @SerializedName("language")
    val language: Language = Language()
)
