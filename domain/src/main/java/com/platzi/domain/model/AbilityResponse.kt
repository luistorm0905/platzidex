package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class AbilityResponse(
    @SerializedName("flavor_text_entries")
    val flavorTextEntries: List<FlavorTextEntry> = listOf(),
    @SerializedName("name")
    val name: String = "",
    var index: Int = 0
)
