package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class TypeFilterResponse(
    @SerializedName("pokemon")
    val pokemon: List<ItemTypeFilter> = listOf()
)
