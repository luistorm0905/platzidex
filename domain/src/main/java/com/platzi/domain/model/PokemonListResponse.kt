package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class PokemonListResponse(
    @SerializedName("next")
    val next: String = "",
    @SerializedName("results")
    val results: List<PokemonListItem> = listOf()
)
