package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class EvolutionChainResponse(
    @SerializedName("chain")
    val chain: EvolvesTo = EvolvesTo()
)