package com.platzi.domain.model

import com.google.gson.annotations.SerializedName

data class ItemTypeFilter(
    @SerializedName("pokemon")
    val pokemonListItem: PokemonListItem = PokemonListItem()
)
