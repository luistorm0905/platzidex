package com.platzi.domain.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}