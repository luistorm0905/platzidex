package com.platzi.domain.repository

import com.platzi.domain.model.*

interface PokeApiRepository {

    suspend fun getPokemonList(): PokemonListResponse

    suspend fun getSpecificPokemonInfo(url: String): Pokemon

    suspend fun getSpeciesInfo(url: String): SpeciesResponse

    suspend fun getAbilitiesInfo(url: String): AbilityResponse

    suspend fun getEvolutionChain(url: String): EvolutionChainResponse

    suspend fun getPokemonByType(typeName: String): TypeFilterResponse

}